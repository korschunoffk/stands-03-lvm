# stands-03-lvm


Файл Result получился после выполнения домашнего задания по методичке, вывод 
команды lsblk с виртуальной машины. Уменьшен размер root до 8Гб , Var в зеркале,
home - под снэпшоты.

```
NAME                       MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                          8:0    0   40G  0 disk 
├─sda1                       8:1    0    1M  0 part 
├─sda2                       8:2    0    1G  0 part /boot
└─sda3                       8:3    0   39G  0 part 
  ├─VolGroup00-LogVol00    253:0    0    8G  0 lvm  /
  ├─VolGroup00-LogVol01    253:1    0  1.5G  0 lvm  [SWAP]
  └─VolGroup00-LogVol_Home 253:2    0    2G  0 lvm  /home
sdb                          8:16   0   10G  0 disk 
sdc                          8:32   0    2G  0 disk 
├─vg_var-lv_var_rmeta_0    253:3    0    4M  0 lvm  
│ └─vg_var-lv_var          253:7    0  952M  0 lvm  /var
└─vg_var-lv_var_rimage_0   253:4    0  952M  0 lvm  
  └─vg_var-lv_var          253:7    0  952M  0 lvm  /var
sdd                          8:48   0    1G  0 disk 
├─vg_var-lv_var_rmeta_1    253:5    0    4M  0 lvm  
│ └─vg_var-lv_var          253:7    0  952M  0 lvm  /var
└─vg_var-lv_var_rimage_1   253:6    0  952M  0 lvm  
  └─vg_var-lv_var          253:7    0  952M  0 lvm  /var
sde                          8:64   0    1G  0 disk
```



# Доп. задание - работа с zfs

Как видно из предыдущего файла, остались 2 раздела /dev/sdb и /dev/sde, 
соответственно их и будем использовать под наши нужды.

В файле homework3.1 сохранены все выводы команд.
# Установка ZFS

узнаем версию релиза ОС
`cat /etc/redhat-release `

> 7.5  

Добавляем необходимый репозиторий
`yum install http://download.zfsonlinux.org/epel/zfs-release.el7_5.noarch.rpm`

Необходимо отключить DKMS репозиторий по умолчанию и подключить kABI репозиторий
`nano /etc/yum.repos.d/zfs.repo`

> В секции zfs [zfs] меняем параметр enabled=1 на enabled=0, 
> В секции zfs-kmod [zfs-kmod] меняем параметр enabled=0 на enabled=1

Сохраняем. 

Устанавливаем ZFS 
`yum install zfs`

Перезапускаемся
`reboot`

Загружаем модули ядра zfs
`modprobe zfs`

Удостоверяемся, что всё работает
`lsmod | grep zfs`

# Создание Zpool

`zpool create -m /opt tank /dev/sdb cache /dev/sde `

Т.к. каталог /opt пустой - сразу монтируем в него пул с именем tank, у которого 
основной диск /dev/sdb и кэш /dev/sde

проверяем что получилось командой 
> zpool iostat -v 

```
              capacity     operations     bandwidth 
pool        alloc   free   read  write   read  write
----------  -----  -----  -----  -----  -----  -----
tank         388K  9.94G      0      0     13    207
  sdb        388K  9.94G      0      0     13    207
cache           -      -      -      -      -      -
  sde       9.50K  1009M      0      0      3      2
----------  -----  -----  -----  -----  -----  -----
```


и командой 
> df -h  

```
Filesystem                          Size  Used Avail Use% Mounted on
/dev/mapper/VolGroup00-LogVol00     8.0G  775M  7.3G  10% /
devtmpfs                            110M     0  110M   0% /dev
tmpfs                               118M     0  118M   0% /dev/shm
tmpfs                               118M  8.5M  110M   8% /run
tmpfs                               118M     0  118M   0% /sys/fs/cgroup
/dev/sda2                          1014M   61M  954M   6% /boot
/dev/mapper/VolGroup00-LogVol_Home  2.0G   33M  2.0G   2% /home
/dev/mapper/vg_var-lv_var           922M  139M  719M  17% /var
tank                                9.7G  128K  9.7G   1% /opt
```


# Создать снэпшот 

`zfs snapshot  tank@first` (имя пула@имя снэпшота)

посмотреть список снэпшотов 

```
zfs list -t snapshot

NAME          USED  AVAIL  REFER  MOUNTPOINT
tank@first     14K      -    31K  -
```


# Откатиться на снэпшот

`zfs rollback tank@first`

можно откатываться на горячую.


